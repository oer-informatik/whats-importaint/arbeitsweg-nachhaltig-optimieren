## What's importaint? Nachhaltige Arbeitswege!

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110884244102345298</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/arbeitsweg-nachhaltig-optimieren</span>



> **tl/dr;** _(ca. 5 min Lesezeit): Was ist wirklich wichtig? Gute Informatiker*innen denken über Systemgrenzen hinaus - und sorgen sich entsprechend um eine nachhaltige Lebensweise. Der berufliche Alltag fängt mit unserem Weg ins Büro an: Wer ohne Auto ins Büro fährt, kann bis zu einer Tonne $CO_2$ pro Jahr einsparen. Wäre es nicht an der Zeit, das nicht nur zu wissen, sondern endlich damit anzufangen?_

Derzeit emittieren wir in Deutschland etwa 10 Tonnen $CO_2$ pro Kopf. Um die dadurch verursachte Erderwärmung auf 2 °C zu begrenzen, muss dieses Budget auf etwa 3 Tonnen $CO_2$ pro Kopf reduziert werden.^[[Ein festes CO2-Budget pro Kopf – wie ginge das? (Bayerischer Rundfunk, 25.01.2023)](https://www.br.de/nachrichten/wissen/ein-festes-co2-budget-pro-kopf-wie-ginge-das,TTO03yy)]

Dazu müssen viele lange bekannte Änderungen politisch umgesetzt werden, aber hier soll es nicht um das große Ganze gehen. Denn auch im Kleinen verändern wir den Markt, unser Umfeld, die Gesellschaft. Aus dem Vorgehensmodell Software-Kanban kennen wir _Kaizen_, das ständige Hinterfragen und kontinuierliche Verbessern. Wer jede Woche seine beruflichen Prozesse und Produkte kontinuierlich vorantreibt, kann auch jede Woche ein bisschen nachhaltiger werden. Diese Woche zum Beispiel nachhaltiger auf unserem Weg ins Büro:

## Wie relevant ist Berufspendeln?

Hat der Weg zur Arbeit überhaupt nennenswerten Einfluss auf unsere persönliche $CO_2$-Bilanz? Überschlagen wir einfach: Angenommen, wir fahren den Weg zur Arbeit 230 Mal (bei 30 Tagen Jahresurlaub). Durchschnittlich ist der Arbeitsweg von Berufstätigen in Deutschland etwa 17 Kilometer lang.^[https://www.spiegel.de/karriere/pendeln-in-deutschland-nehmen-immer-mehr-menschen-lange-wege-zum-arbeitsplatz-in-kauf-a-085c2c3a-36ef-4aeb-b807-6fbc70e5d95d] Jährlich ergeben sich so 7820 Kilometer Hin- und Rückwege.

Fahren wir diese Strecke allein in einem mittelalten Benziner, so emittieren wir ca. 1565 Kilogramm $CO_2$ pro Kopf und Jahr. Mehr als eine Tonne (!) - also etwa ein Siebtel der durchschnittlichen Emissionen allein für den Weg zur Arbeit!

Wer ein e-Auto fährt, darf sich indes nicht zurücklehnen: hier bleiben etwa 1066 Kilogramm $CO_2$ pro Kopf und Jahr. Bei einem e-Bike sind es noch 31 Kilogramm $CO_2$ pro Kopf und Jahr. Wer gerne selbst ein bisschen herumrechnen will, dem sei z.B. der [Rechner von "Quarks und Co."](https://www.quarks.de/umwelt/klimawandel/co2-rechner-fuer-auto-flugzeug-und-co/) empfohlen und vor allem die Erläuterungen auf dieser Seite.

![Screenshot des CO2-Rechners von ["Quarks und Co."](https://www.quarks.de/umwelt/klimawandel/co2-rechner-fuer-auto-flugzeug-und-co/) Lizenz unbekannt, Quelle: WDR/Quarcks und Co.](images/quarcks-und-Co-Screenshot.png)

Letztlich ist es aber fast egal, wie viel $CO_2$ der Rechner individuell ermittelt: der Umstieg auf eine nachhaltigere Fahrt zur Arbeit hat beispielsweise soziale (Vorbildfunktion), gesundheitliche (Senkung des Krebs- und Herz-Kreislauf-Krankheitsrisikos^[Studie der Universität Glasgow beobachtet 40% gemindertes Risiko bei Fahrradpendlern](https://www.aerzteblatt.de/nachrichten/74268/Radfahrer-pendeln-gesuender-Niedrigeres-Krebs-und-Sterberisiko-in-Studie)) und finanzielle Vorteile. Welche Optionen es gibt, liegt auf der Hand, trotzdem seinen sie hier nochmals erwähnt:

## Für Wege bis zehn Kilometer (eine Strecke): Das Fahrrad!

Herzlichen Glückwunsch! Du gehörst damit zur glücklichen Hälfte (48%) der Berufspendler*innen, die relativ nah am Arbeitsplatz wohnen. Etwa die Hälfte davon (27%) wohnt gerade einmal fünf Kilometer entfernt (Zahlen von 2020).

Fünf Kilometer, eine Strecke die jede/r noch so ungeübte Fahrradfahrende in zwanzig Minuten schafft - und zwar gemütlich, ohne zu schwitzen und auf einer alten Möhre als Fahrrad. Im Jahr 2020 waren es noch 40% (!), die bei dieser Streckelänge das Auto genutzt haben. Ich gehe davon aus, dass es heute weniger Autofahrende gibt. Corona hat in vielen die Lust am Rad neu geweckt - beispielsweise aus gesundheitlichen Gründen oder weil es sich finanziell lohnt. 

Bis zu zehn Kilometer lassen sich leicht - auch für ungeübte - mit dem Fahrrad fahren. Wenn die Gegend nicht sehr bergig ist, schafft man das auch gut in 40 Minuten. Trotzdem nutzen etwa 69% der beruflich Pendelnden, die bis zu zehn Kilometer von der Arbeitsstätte entfernt wohnen, das Auto. ^[[Statistisches Bundesamt: Berufspendelnde nutzen auch für kurze Arbeitswege am häufigsten das Auto (zlt bes. am 13.8.23)](https://www.destatis.de/DE/Themen/Arbeit/Arbeitsmarkt/Erwerbstaetigkeit/aktuell-erwerbstaetigkeit.html)]

![DALL·E Bild: ein Fahrrad vor rotem Hintergrund (ein Wagon eines Zugs)](images/DALL·E2.jpg)

Dafür braucht es auch kein großes Equipment: für die ersten Wochen tut es eine Tasche mit Wechselklamotten auf dem Gepäckträger und ein Deo im Gepäck. Wenn man sich dann auf dem Hinweg etwas mehr Zeit lässt, kann man auf dem Rückweg immer noch sprinten - sofern man die Fahrt unter "Sport" verbuchen will.

Die Erfahrung zeigt: Wer erst einmal angefangen hat, mit dem Rad zur Arbeit zu fahren, der hört so schnell nicht mehr auf. Nach dem Sommer fährt man dann automatisch mit Regenhose, Schuh-Überzieher und Handschuhen bei jedem Wind und Wetter! Wer will sich denn auch wieder hinten im Stau anstellen, wenn man weiß, wie angenehm es ohne geht!

## Wege bis 25 Kilometer (eine Strecke): Wie wäre es mit einem e-Bike?

Bei längeren Strecken ist das Fahrrad ("Bio-Bike") für die meisten keine Alternative mehr. Wer die frische Luft und die Bewegung nicht missen will, für den ist ein E-Bike der nächste logische Schritt. Geht man von einer einfachen Streckendauer von bis zu einer Stunde aus, so lassen sich mit dem E-Bike Entfernungen zur Arbeit von bis zu 25 Kilometern gut überbrücken. 

Für die ersten Versuche sollte man sich zunächst ein E-Bike leihen - schließlich sind die Räder ziemlich teuer. Ein altes Rad tut es auch: Die erste E-Bike-Generation bekommt man gebraucht für 400 €, mit etwas Glück tut's der Akku noch gut genug. Ein neuer Akku kostet nochmal 400€ - viel Geld, aber eben auch nicht so viel, wie ein neuer Zahnriemen am Auto.

## Nachfragen lohnt: Wie wäre ein Dienstfahrrad?

Viele Firmen bieten ihren Angestellten analog zu Dienstwägen mittlerweile auch Dienstfahrräder an. Über [jobrad.org](https://www.jobrad.org/) kann man sich die Kosten und Einsparungen berechnen. 

![DALL·E Bild: ein schickes Fahrrad mit Ledersattel](images/DALL·E1.jpg)

Es lohnt, sich im Betrieb mal über die Möglichkeiten zu erkundigen - oder sich dafür einzusetzen, dass Diensträder eingeführt werden. Für den Arbeitgeber ist es eine Win-Win-Situation - schließlich profitiert er finanziell (steuermindernde Betriebsausgabe), nochmal finanziell (im Schnitt zwei Krankheitstage weniger bei Fahrradfahrern), bindet die Angestellten an das Unternehmen, pflegt das Image, erfüllt selbstgesetzte Nachhaltigkeitsziele.^[Infos zu Diensträdern z.B. beim [VCD](https://www.vcd.org/artikel/steuerliche-vorteile-fuer-dienstfahrraeder/)]

## Stadt- und Regionalverkehr

Hier ist im letzten Jahr natürlich am meisten passiert. Durch das Deutschlandticket haben viele den Öffentlichen Personennahverkehr (ÖPNV) mal ausprobiert. Auch hier gilt: wer es nicht ein, zwei Wochen ohne Kompromisse versucht, wird nicht Umsteigen. Einige Vorteile ergeben sich erst mit der Zeit, einige Strecken findet man erst später raus. Pluspunkt: man kann während der Fahrt lesen, Podcast hören, Mails beantworten, (Klassenarbeiten korrigieren... ja, ich bin da mittlerweile etwas abgebrüht). Zeit im ÖPNV ist Quality-Time.

Ein kleiner Hinweis dazu noch: Es lohnt, nachzuschauen, ob das regionale Verkehrsunternehmen beim Kauf eines Deutschlandtickets weitere Zusatzangebote bietet: Ermäßigung beim Carsharing, Fahrradverleih, Rollerverleih, Mitnahmeregelungen, Nachtbus. In Bielefeld ist das beispielsweise recht attraktiv.

## Die Kombination: Faltrad, Radstation, Park & Ride...

Manchmal muss man für das Optimum auch das Verkehrsmittel kombinieren. Wer teure Räder fährt oder ein Fahrrad am arbeitsnahen Bahnhof dauerparken will, der sollte ich mal mit den Fahrraddepots vertraut machen. Einige Bahnhöfe bieten größere Fahrradparkhäuser, andere kleine abgeschlossene Fahrradboxen. Ein trockener Sattel allein ist oft das wenige Geld schon wert.

Das Fahrrad in den Zug/in die Bahn mitzunehmen ist in manchen Regionalzügen relativ problemlos möglich. Mitten in der Rushhour kann es aber etwas eng werden. Außerdem kosten Fahrräder extra und können in Fernverkehrszügen nicht ohne Reservierung mitgenommen werden.

Keine Mehrkosten verursachen Klapp- und Falträder. Einige ältere Modelle sind auch recht günstig gebraucht zu kaufen. Sie vereinfachen die Fahrradmitnahme enorm. Wer etwas mehr Geld in die Hand nehmen kann, der bekommt mit noch kompakteren Falträdern auch fahrtechnisch gute Alternativen zu großen Rädern.

In größeren Städten lohnt es, sich nach Park&Ride Parkplätzen zu erkundigen - so bleibt das Auto immerhin außerhalb der Innenstadt. 

## Fahrgemeinschaften

Manchmal geht es wirklich nicht anders und das Auto ist nun mal der einzige Weg, zur Arbeit zu kommen. Leider ist es mittlerweile fast unüblich geworden, zur Arbeit Fahrgemeinschaften zu bilden. Aber vielleicht lässt es sich in größeren Betrieben wieder einführen? Damit lassen sich auf einen Schlag die Emissionen fast halbieren.

## Fazit: Alle Optionen versuchen und Stück für Stück verbessern

Bei den wenigsten wird es gar keine Möglichkeit geben, den Arbeitsweg nachhaltiger zu gestalten. Oft ist der schwerste Punk, sich einfach zu überwinden, und einfach mal eine neue Option (Rad, ÖPNV) zu probieren. 

Ich habe allerlei Arbeitswege hinter mir - innerhalb Berlins mit Rad oder ÖPNV, mit dem Rad und der Bahn von Provinzwohnung zu Provinzbüro, Autobahnpendeln über 50km, E-Bike bei 23km. Für die Strecken habe ich zwischen einer halben und 1,5 Stunden gebraucht. Nur im Auto kam es mir vor wie verlorene Zeit - die Podcasts nervten nach einiger Zeit, die anderen Autofahrer sowieso. 

Im Zug konnte ich lesen, schlafen, arbeiten. Am besten waren aber immer die Fahrradstrecken: Wer nach 30min Fahrradfahren Zuhause ankommt, lässt allen Stress des Büros auf der Strecke. Das hilft auch der mentalen Gesundheit.

Es gibt genau einen Weg herauszufinden, was einem guttut: es auszuprobieren. Uns bleibt nichts anderes übrig, als nachhaltiger zu werden. Und diese Woche ist ein guter Zeitpunkt, damit wieder ein Stück vorwärtszukommen!

## Lesetipps

Wer mehr zum Thema erfahren will, der sollte sich dringen das Buch "Autokorrektur" von Katja Diehl kaufen: Sie beleuchtet Mobilität aus vielen unterschiedlichen Blickwinkeln, allesamt interessant, viele mir vorher unbekannt. [Verlag: S. FISCHER , ISBN: 978-3-10-397142-2](https://www.fischerverlage.de/buch/katja-diehl-autokorrektur-mobilitaet-fuer-eine-lebenswerte-welt-9783103971422)

